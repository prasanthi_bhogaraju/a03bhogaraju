var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");
var server = require('http').createServer(app);  // use our app to create a server
var io = require('socket.io')(server); // pass in our http app server to get a Socket.io server
var path = require('path');
var app = express();  // make express app
var http = require('http').Server(app);


// on a GET request to default page, do this.... 
app.get('/', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Bhogaraju/assets', 'Home.html'));
});
// set up the view engine
app.use(express.static(__dirname + '/assets'));
app.set("assets", path.resolve(__dirname, "/assets")); // path to assets
app.set("view engine", "ejs"); // specify our view engine



 
// manage our entries
var entries = [];
app.locals.entries = entries;

// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));

// http GET (default and /new-entry)
app.get("/guestbook", function (request, response) {
  response.render("index");
});
app.post("/guestbook", function (request, response) {
  response.render("index");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

// http POST (INSERT)
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/guestbook");
  res.render('index');
});

// 404
app.use(function (request, response) {
  response.status(404).render("404");
});

// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/');
});
