# W07 Guestbook Example

A simple guestbook app using:

-Node.js platform
-Express web framework
-BootStrap framework


> npm install
> node server.js
```

Point your browser to `http://localhost:8081`. 

## Reference

Express in Action: Writing, building, and testing Node.js applications
by Evan M. Hahn

https://www.manning.com/books/express-in-action




